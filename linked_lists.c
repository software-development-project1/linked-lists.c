

#include <stdio.h>
#include <stdlib.h>

struct Node {
    int data;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);
void printList(struct Node *head);

int main() {
    struct Node *head = NULL;
    int choice, data, key, value, searchValue, newValue;

    while (1) {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: ");
                scanf("%d", &key);
                deleteByKey(&head, key);
                break;
            case 5:
                printf("Enter value to delete: ");
                scanf("%d", &value);
                deleteByValue(&head, value);
                break;
            case 6:
                printf("Enter key after which to insert: ");
                scanf("%d", &key);
                printf("Enter value to insert: ");
                scanf("%d", &value);
                insertAfterKey(&head, key, value);
                break;
            case 7:
                printf("Enter search value after which to insert: ");
                scanf("%d", &searchValue);
                printf("Enter new value to insert: ");
                scanf("%d", &newValue);
                insertAfterValue(&head, searchValue, newValue);
                break;
            case 8:
                printf("Exiting the program...\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

// Function to create a new node
struct Node *createNode(int num) {
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }
    newNode->data = num;
    newNode->next = NULL;
    return newNode;
}

// Function to append a new node to the end of the linked list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
    } else {
        struct Node *current = *head;
        while (current->next != NULL) {
            current = current->next;
        }
        current->next = newNode;
    }
}

// Function to prepend a new node to the beginning of the linked list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to delete a node by key from the linked list
void deleteByKey(struct Node **head, int key) {
    struct Node *current = *head;
    struct Node *prev = NULL;
    while (current != NULL && current->data != key) {
        prev = current;
        current = current->next;
    }
    if (current != NULL) {
        if (prev == NULL) {
            *head = current->next;
        } else {
            prev->next = current->next;
        }
        free(current);
    }
}

// Function to delete nodes by value from the linked list
void deleteByValue(struct Node **head, int value) {
    struct Node *current = *head;
    struct Node *prev = NULL;
    while (current != NULL) {
        if (current->data == value) {
            if (prev == NULL) {
                *head = current->next;
            } else {
                prev->next = current->next;
            }
            struct Node *temp = current;
            current = current->next;
            free(temp);
        } else {
            prev = current;
            current = current->next;
        }
    }
}

// Function to insert a new node after a key
void insertAfterKey(struct Node **head, int key, int value) {
    struct Node *current = *head;
    while (current != NULL) {
        if (current->data == key) {
            struct Node *newNode = createNode(value);
            newNode->next = current->next;
            current->next = newNode;
            break;
        }
        current = current->next;
    }
}

// Function to insert a new node after a value
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *current = *head;
    while (current != NULL) {
        if (current->data == searchValue) {
            struct Node *newNode = createNode(newValue);
            newNode->next = current->next;
            current->next = newNode;
            break;
        }
        current = current->next;
    }
}

// Function to print the linked list
void printList(struct Node *head) {
    struct Node *current = head;
    while (current != NULL) {
        printf("%d ", current->data);
        current = current->next;
    }
    printf("\n");
}
